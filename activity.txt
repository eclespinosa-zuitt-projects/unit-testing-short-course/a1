E-commerce applications
    Features:
        1. Online payment using credit card, debit card
        2. Shipping information
        3. Adding the products to cart
        4. Buy/Checking out
        5. Tracking parcel

    
    Online Payment Features
        Form Validation
            1. credit card number
                - check if all number
                - check if the credit card number is correct (can use luhn algorithm)
                - numbers should be 13 to 19 digits
            
            2. name on the card
                - check if not empty
                - check if not undefined
                - check if not null
            
            3. Billing address
                - check if not empty
                - check if not undefined
                - check if not null
                - check if the address is not made-up (at least the country, state, province and city)
            
            4. CVV number
                - check if not empty
                - check if not undefined
                - check if not null
                - should be a number
                - have 3 (for VISA, MasterCard) or 4 (for American Express) digits

    Shipping Information Features
        Form Validation
            1. Address
                - check if not empty
                - check if not undefined
                - check if not null
                - check if the address is not made-up (at least the country, state, province and city)
            
            2. Name
                - check if not empty
                - check if not undefined
                - check if not null
            
            3. Number
                - check if not empty
                - check if not undefined
                - check if not null
                - should be all numbers
                - must have 11 digits
            
            4. Postal code
                - check if not empty
                - check if not undefined
                - check if not null
                - should be all numbers (if within PH)
    
    Add to Cart Features
            1. Product
                - check if product exist
                - check if not out-of-stock
                - check if the product is already in the cart

    Buy Features
        1. products
            - check if the products is not empty
            - check if the products exist
            - check if the products is available

        2. voucher
            form validation
                - check if undefined or not
                - check if voucher is valid

        3. Shipping Information
            - check if the address is not undefined
            - check if the address information is on the address feature
            - check if the courier is undefined/null

        4. Payment method
            form validation
                - check if not null
                - check if not undefined

    Tracking parcel
        1. Orders
            - Check if the user has pending order

        2. Tracking number
            Form Validation
                - Check if undefined
                - check if null
                - check if the number is valid
                - check if the number exists

